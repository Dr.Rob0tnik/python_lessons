# Пустой список
num_list = []

# Получаем числа на ввод и записываем их в список
num_list.append(int(input("enter number 1: ")))
num_list.append(int(input("enter number 2: ")))
num_list.append(int(input("enter number 3: ")))

# Сортируем список
num_list.sort()

# Выводим поочередно каждое число из списка
for i in range(len(num_list)):
    print(num_list[i])
